import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-resetear',
  templateUrl: './resetear.component.html',
  styleUrls: ['./resetear.component.sass']
})
export class ResetearComponent implements OnInit {

  @Input() valor: number = 1;
  @Output() nuevoValor = new EventEmitter<number>();

  constructor() { }

  reset(){
    this.valor = 0
    this.nuevoValor.emit(0)
  }

  ngOnInit(): void {

  }

}
