import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-multiplicar-dividir',
  templateUrl: './multiplicar-dividir.component.html',
  styleUrls: ['./multiplicar-dividir.component.sass']
})
export class MultiplicarDividirComponent implements OnInit {

  @Input() valor: number = 1;
  @Output() nuevoValor = new EventEmitter<number>();

  constructor() { }

  multiplicar(){
    this.nuevoValor.emit(this.valor=this.valor*2);
  }

  dividir(){
    this.nuevoValor.emit(this.valor=this.valor/2);
  }

  recibe(event:number){
    this.valor = event
    this.nuevoValor.emit(this.valor)
  }

  ngOnInit(): void {
  }

}
