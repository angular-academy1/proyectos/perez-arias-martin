import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-incrementar-decrementar',
  templateUrl: './incrementar-decrementar.component.html',
  styleUrls: ['./incrementar-decrementar.component.sass']
})
export class IncrementarDecrementarComponent implements OnInit {

  @Input() valor: number = 1;
  @Output() nuevoValor = new EventEmitter<number>();

  constructor() { }

  incrementar(){
    this.nuevoValor.emit(this.valor += 1);
  }

  decrementar(){
    this.nuevoValor.emit(this.valor -=1);
  }

  recibe(event:number){
    this.valor = event
    this.nuevoValor.emit(this.valor)
  }

  ngOnInit(): void {
  }

}
